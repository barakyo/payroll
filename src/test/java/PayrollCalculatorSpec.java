import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by barackkaravani on 8/22/16.
 */
public class PayrollCalculatorSpec {
    private PayrollCalculator calculator;

    @Before
    public void setup() {
        this.calculator = new PayrollCalculator();
    }

    @Test
    public void testNetPay() {
        assertEquals(88.0, calculator.calculateNetPay(10.0, 10.0, 5.00, 7.00));
    }
}
